﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptocurrencyMarket.Model.Bx
{
    public class THB
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class BTC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class ETH
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class BCH
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class DAS
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class DOG
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class EVX
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class FTC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class GNO
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class HYP
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class LTC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class NMC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class OMG
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class PND
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class POW
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class PPC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class QRK
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class REP
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class XCN
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class XPM
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class XPY
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class XRP
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class XZC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class ZEC
    {

        [JsonProperty("total")]
        public string Total { get; set; }

        [JsonProperty("available")]
        public string Available { get; set; }

        [JsonProperty("orders")]
        public string Orders { get; set; }

        [JsonProperty("withdrawals")]
        public string Withdrawals { get; set; }

        [JsonProperty("deposits")]
        public string Deposits { get; set; }

        [JsonProperty("options")]
        public string Options { get; set; }
    }

    public class Balance
    {

        [JsonProperty("THB")]
        public THB THB { get; set; }

        [JsonProperty("BTC")]
        public BTC BTC { get; set; }

        [JsonProperty("ETH")]
        public ETH ETH { get; set; }

        [JsonProperty("BCH")]
        public BCH BCH { get; set; }

        [JsonProperty("DAS")]
        public DAS DAS { get; set; }

        [JsonProperty("DOG")]
        public DOG DOG { get; set; }

        [JsonProperty("EVX")]
        public EVX EVX { get; set; }

        [JsonProperty("FTC")]
        public FTC FTC { get; set; }

        [JsonProperty("GNO")]
        public GNO GNO { get; set; }

        [JsonProperty("HYP")]
        public HYP HYP { get; set; }

        [JsonProperty("LTC")]
        public LTC LTC { get; set; }

        [JsonProperty("NMC")]
        public NMC NMC { get; set; }

        [JsonProperty("OMG")]
        public OMG OMG { get; set; }

        [JsonProperty("PND")]
        public PND PND { get; set; }

        [JsonProperty("POW")]
        public POW POW { get; set; }

        [JsonProperty("PPC")]
        public PPC PPC { get; set; }

        [JsonProperty("QRK")]
        public QRK QRK { get; set; }

        [JsonProperty("REP")]
        public REP REP { get; set; }

        [JsonProperty("XCN")]
        public XCN XCN { get; set; }

        [JsonProperty("XPM")]
        public XPM XPM { get; set; }

        [JsonProperty("XPY")]
        public XPY XPY { get; set; }

        [JsonProperty("XRP")]
        public XRP XRP { get; set; }

        [JsonProperty("XZC")]
        public XZC XZC { get; set; }

        [JsonProperty("ZEC")]
        public ZEC ZEC { get; set; }
    }

    public class GetBalance
    {

        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("balance")]
        public Balance Balance { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }


}