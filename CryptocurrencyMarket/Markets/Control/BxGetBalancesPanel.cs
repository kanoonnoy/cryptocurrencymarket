﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.Text;
using System.Net.Http;
using OtpNet;
using System.Net;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Reflection;

namespace CryptocurrencyMarket.Markets.Control
{
    public partial class BxGetBalancesPanel : UserControl
    {
        private bool _Istartup = true;

        private string _UrlGetBalance = "https://bx.in.th/api/balance/";
        private string _ApiKey = string.Empty;
        private int _Nonce = 0;
        private string _Signature = string.Empty;
        private string _Secretkey = string.Empty;
        private string _2factor = string.Empty;
        private string _SecurityKey = string.Empty;
        private static readonly HttpClient client = new HttpClient();

        private DataTable _dtSource = new DataTable();
        public BxGetBalancesPanel()
        {
            InitializeComponent();
            this.label8.Text = string.Empty;

#if !DEBUG
                 this.textBox2.Text= "";
                this.textBox3.Text= "";
                 this.textBox4.Text= "";
                this.textBox6.Text = "";
                this.textBox5.Text = _Nonce.ToString("N0");
                this.label7.Text = "";
#endif
        }


        public  string getHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        public void Reload()
        {
            try
            {
                this.Enabled = false;
                this.Cursor = Cursors.WaitCursor;


                if (_Istartup)
                {
                    _Nonce = 0;
                    _Istartup = false;

                    _dtSource.Columns.Add("Coin", typeof(string));
                    _dtSource.Columns.Add("Total", typeof(decimal));
                    _dtSource.Columns.Add("Available", typeof(decimal));
                    _dtSource.Columns.Add("Orders", typeof(decimal));
                    _dtSource.Columns.Add("Withdrawals", typeof(decimal));
                    _dtSource.Columns.Add("Deposits", typeof(decimal));
                    _dtSource.Columns.Add("Options", typeof(decimal));

                }

                _UrlGetBalance = this.textBox1.Text;
                _ApiKey = this.textBox2.Text;
                _Secretkey = this.textBox3.Text;
                _SecurityKey = this.textBox4.Text;
                _2factor = string.Empty;

                if(string.IsNullOrEmpty(_UrlGetBalance) || string.IsNullOrEmpty(_ApiKey) || string.IsNullOrEmpty(_Secretkey))
                {
                    MessageBox.Show("Input cannot be empty!");
                    return;
                }

                this.textBox6.Text = _Signature;
                this.textBox5.Text = _Nonce.ToString("N0");
                this.label7.Text = "";

                Console.WriteLine(DateTime.Now.Ticks);
                // Post
                var resultJson = string.Empty;
                //string JSON_Location = @"D:\json\test.json";
                //resultJson = System.IO.File.ReadAllText(JSON_Location);

                this.label8.Text = string.Empty;
                

                while (true)
                {
                    // Signature
                    if (_dtSource.Rows.Count > 0) _dtSource.Rows.Clear();
                    _Nonce += 1;
                    _Signature = getHashSha256(string.Format("{0}{1}{2}", _ApiKey, _Nonce, _Secretkey));
                    this.textBox5.Text = _Nonce.ToString("N0");
                    this.textBox6.Text = _Signature;

                    // 2Factor
                    if (!string.IsNullOrEmpty(_SecurityKey))
                    {
                        try
                        {
                            var totp = new Totp(Base32Encoding.ToBytes(_SecurityKey));
                            _2factor = totp.ComputeTotp();
                            this.label7.Text = _2factor;
                        }
                        catch (Exception ex )
                        {
                            this.label7.Text = string.Format("error:{0}",ex.Message);
                        }
                    }
                    using (WebClient client = new WebClient())
                    {
                        var value = new NameValueCollection();
                        value.Add("key", _ApiKey);
                        value.Add("nonce", _Nonce.ToString());
                        value.Add("signature", _Signature);

                        if (!string.IsNullOrEmpty(_2factor)) value.Add("twofa", _2factor);

                        var response = client.UploadValues(_UrlGetBalance, value);
                        resultJson = System.Text.Encoding.UTF8.GetString(response);
                        Console.WriteLine(resultJson);
                    }

                    if (!string.IsNullOrEmpty(resultJson))
                    {
                        var objects = JObject.Parse(resultJson); // parse as object
                        var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Model.Bx.GetBalance>(objects.ToString());
                        if (!result.Success)
                        {
                            if(result.Error != "Invalid Nonce")
                            {
                                // Next
                                MessageBox.Show(result.Error);
                                return;

                            }
                            //MessageBox.Show(string.Format("Error : {0}", result.Error), "Warring", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //return;
                        }
                        else
                        {
                            var balance = result.Balance;
                            _dtSource.Rows.Add("THB", balance.THB.Total, balance.THB.Available, balance.THB.Orders, balance.THB.Withdrawals, balance.THB.Deposits, balance.THB.Options);
                            _dtSource.Rows.Add("BTC", balance.BTC.Total, balance.BTC.Available, balance.BTC.Orders, balance.BTC.Withdrawals, balance.BTC.Deposits, balance.BTC.Options);
                            _dtSource.Rows.Add("ETH", balance.ETH.Total, balance.ETH.Available, balance.ETH.Orders, balance.ETH.Withdrawals, balance.ETH.Deposits, balance.ETH.Options);
                            _dtSource.Rows.Add("BCH", balance.BCH.Total, balance.BCH.Available, balance.BCH.Orders, balance.BCH.Withdrawals, balance.BCH.Deposits, balance.BCH.Options);
                            _dtSource.Rows.Add("DAS", balance.DAS.Total, balance.DAS.Available, balance.DAS.Orders, balance.DAS.Withdrawals, balance.DAS.Deposits, balance.DAS.Options);
                            _dtSource.Rows.Add("DOG", balance.DOG.Total, balance.DOG.Available, balance.DOG.Orders, balance.DOG.Withdrawals, balance.DOG.Deposits, balance.DOG.Options);
                            _dtSource.Rows.Add("EVX", balance.EVX.Total, balance.EVX.Available, balance.EVX.Orders, balance.EVX.Withdrawals, balance.EVX.Deposits, balance.EVX.Options);
                            _dtSource.Rows.Add("FTC", balance.FTC.Total, balance.FTC.Available, balance.FTC.Orders, balance.FTC.Withdrawals, balance.FTC.Deposits, balance.FTC.Options);
                            _dtSource.Rows.Add("GNO", balance.GNO.Total, balance.GNO.Available, balance.GNO.Orders, balance.GNO.Withdrawals, balance.GNO.Deposits, balance.GNO.Options);
                            _dtSource.Rows.Add("HYP", balance.HYP.Total, balance.HYP.Available, balance.HYP.Orders, balance.HYP.Withdrawals, balance.HYP.Deposits, balance.HYP.Options);
                            _dtSource.Rows.Add("LTC", balance.LTC.Total, balance.LTC.Available, balance.LTC.Orders, balance.LTC.Withdrawals, balance.LTC.Deposits, balance.LTC.Options);
                            _dtSource.Rows.Add("NMC", balance.NMC.Total, balance.NMC.Available, balance.NMC.Orders, balance.NMC.Withdrawals, balance.NMC.Deposits, balance.NMC.Options);
                            _dtSource.Rows.Add("OMG", balance.OMG.Total, balance.OMG.Available, balance.OMG.Orders, balance.OMG.Withdrawals, balance.OMG.Deposits, balance.OMG.Options);
                            _dtSource.Rows.Add("PND", balance.PND.Total, balance.PND.Available, balance.PND.Orders, balance.PND.Withdrawals, balance.PND.Deposits, balance.PND.Options);
                            _dtSource.Rows.Add("POW", balance.POW.Total, balance.POW.Available, balance.POW.Orders, balance.POW.Withdrawals, balance.POW.Deposits, balance.POW.Options);
                            _dtSource.Rows.Add("PPC", balance.PPC.Total, balance.PPC.Available, balance.PPC.Orders, balance.PPC.Withdrawals, balance.PPC.Deposits, balance.PPC.Options);
                            _dtSource.Rows.Add("QRK", balance.QRK.Total, balance.QRK.Available, balance.QRK.Orders, balance.QRK.Withdrawals, balance.QRK.Deposits, balance.QRK.Options);
                            _dtSource.Rows.Add("REP", balance.REP.Total, balance.REP.Available, balance.REP.Orders, balance.REP.Withdrawals, balance.REP.Deposits, balance.REP.Options);
                            _dtSource.Rows.Add("XCN", balance.XCN.Total, balance.XCN.Available, balance.XCN.Orders, balance.XCN.Withdrawals, balance.XCN.Deposits, balance.XCN.Options);
                            _dtSource.Rows.Add("XPM", balance.XPM.Total, balance.XPM.Available, balance.XPM.Orders, balance.XPM.Withdrawals, balance.XPM.Deposits, balance.XPM.Options);
                            _dtSource.Rows.Add("XPY", balance.XPY.Total, balance.XPY.Available, balance.XPY.Orders, balance.XPY.Withdrawals, balance.XPY.Deposits, balance.XPY.Options);
                            _dtSource.Rows.Add("XRP", balance.XRP.Total, balance.XRP.Available, balance.XRP.Orders, balance.XRP.Withdrawals, balance.XRP.Deposits, balance.XRP.Options);
                            _dtSource.Rows.Add("XZC", balance.XZC.Total, balance.XZC.Available, balance.XZC.Orders, balance.XZC.Withdrawals, balance.XZC.Deposits, balance.XZC.Options);
                            _dtSource.Rows.Add("ZEC", balance.ZEC.Total, balance.ZEC.Available, balance.ZEC.Orders, balance.ZEC.Withdrawals, balance.ZEC.Deposits, balance.ZEC.Options);

                            this.dataGridView1.DataSource = _dtSource;
                            this.label8.Text = string.Format(this.label8.Tag.ToString(), DateTime.Now);
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }

                    System.Threading.Thread.Sleep(200);
                }
                


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Enabled = true;
                this.Cursor = Cursors.Default;

            }
        }

        private void btGetBalance_Click(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
