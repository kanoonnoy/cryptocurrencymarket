﻿namespace CryptocurrencyMarket.Markets
{
    partial class BxForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BxForm));
            this.bxGetBalancesPanel1 = new CryptocurrencyMarket.Markets.Control.BxGetBalancesPanel();
            this.SuspendLayout();
            // 
            // bxGetBalancesPanel1
            // 
            this.bxGetBalancesPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bxGetBalancesPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.bxGetBalancesPanel1.Location = new System.Drawing.Point(0, 0);
            this.bxGetBalancesPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.bxGetBalancesPanel1.Name = "bxGetBalancesPanel1";
            this.bxGetBalancesPanel1.Size = new System.Drawing.Size(835, 829);
            this.bxGetBalancesPanel1.TabIndex = 0;
            // 
            // BxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 829);
            this.Controls.Add(this.bxGetBalancesPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BxForm";
            this.Text = "BxForm";
            this.ResumeLayout(false);

        }

        #endregion

        private Control.BxGetBalancesPanel bxGetBalancesPanel1;
    }
}